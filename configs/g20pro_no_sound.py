from uinputmapper.cinput import *
from uinputmapper.mapper import pretty_conf_print


"""
Configuration for a G20s pro removing volume up/down keys
"""

# skip volume up and down keys
def config_merge(c, n):
    # pretty_conf_print(c)
    del c[(1, EV_KEY)][KEY_VOLUMEDOWN]
    del c[(1, EV_KEY)][KEY_VOLUMEUP]
